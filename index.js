// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// Import the task routes
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://dbIanFlores:Nitefallbrigade2936@wdc028-course-booking.6gyek.mongodb.net/b138_to-do?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// Add the task route
app.use("/task", taskRoute)

// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`))